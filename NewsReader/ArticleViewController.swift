//
//  ArticleViewController.swift
//  NewsReader
//
//  Created by George Heints on 30.08.17.
//  Copyright © 2017 George Heints. All rights reserved.
//

import UIKit

class ArticleViewController: UIViewController, UIGestureRecognizerDelegate {

    // MARK: - Outlets
    @IBOutlet weak var ArticleViewImg: UIImageView!
    @IBOutlet weak var ArticleViewTitle: UILabel!
    @IBOutlet weak var ArticleViewDesc: UILabel!
    
    var FileViewOrigin: CGPoint!
    var scrollView: UIScrollView!
    
    private var _article: Article!
    
    //Get data from ViewController
    var article: Article{
        get{
            return _article
        } set{
            _article = newValue
        }
    }
    
    //user interraction
    func userInterraction(){
        self.ArticleViewImg.isUserInteractionEnabled = true
        self.ArticleViewImg.isUserInteractionEnabled = true
        ArticleViewDesc.sizeToFit()
        ArticleViewDesc.numberOfLines = 0
        ArticleViewTitle.text = article.headline
        ArticleViewDesc.text = article.descript
        FileViewOrigin = view.frame.origin
    }
    
    //load image from URL
    func addImage(){
        let url = article.imageUrl
        let ArticlePic = URL(string: url!)
        let data = try? Data(contentsOf: ArticlePic!)
        if let imageData = data {
            let image = UIImage(data: data!)
            ArticleViewImg.image = image
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userInterraction()
        //GetData()
        addImage()
    }
    
    @IBAction func pinchImg(_ sender: UIPinchGestureRecognizer) {

    }

    @IBAction func DragedView(_ sender: UIPanGestureRecognizer) {

    }
    //When image tapped
    @IBAction func imgTapped(_ sender: UITapGestureRecognizer) {
        /*
         let newImageView = UIImageView(image: ArticleViewImg.image)
         newImageView.frame = UIScreen.main.bounds
         newImageView.backgroundColor = .black
         newImageView.contentMode = .scaleAspectFit
         newImageView.isUserInteractionEnabled = true
         let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
         newImageView.addGestureRecognizer(tap)
         self.view.addSubview(newImageView)
         let pinch = UIPinchGestureRecognizer(target: self, action: #selector(pinchReconnizer))
         newImageView.addGestureRecognizer(pinch)
         self.navigationController?.isNavigationBarHidden = true
         self.tabBarController?.tabBar.isHidden = true
        
        */
        let newImageView = UIImageView(image: ArticleViewImg.image)
        newImageView.isUserInteractionEnabled = true
        
        
        scrollView = UIScrollView(frame: view.bounds)
        scrollView.backgroundColor = .black
        scrollView.contentSize = newImageView.bounds.size
        scrollView.autoresizingMask = UIViewAutoresizing.flexibleWidth
        scrollView.autoresizingMask = UIViewAutoresizing.flexibleHeight
        scrollView.addSubview(newImageView)
        newImageView.contentMode = .scaleAspectFit
        newImageView.frame = UIScreen.main.bounds
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        scrollView.addGestureRecognizer(tap)
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(pinchReconnizer))
        newImageView.addGestureRecognizer(pinch)
        view.addSubview(scrollView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        scrollView.contentOffset = CGPoint(x: 0, y: 0)
        setupGestureRecognizer()

    }
  
    //dismiss tap
    func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        do{
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
        view.backgroundColor = .white
        ArticleViewImg.isHidden = false
        } catch let error{
            print(error)
        }
    }
    
    //double tap
    func setupGestureRecognizer() {
        let doubleTap = UITapGestureRecognizer(target: self, action: "handleDoubleTap:")
        doubleTap.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(doubleTap)
    }
    
    func handleDoubleTap(recognizer: UITapGestureRecognizer) {
        
        if (scrollView.zoomScale > scrollView.minimumZoomScale) {
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        } else {
            scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
        }
    }
    
    //pinch
    func pinchReconnizer(_ sender: UIPinchGestureRecognizer){
        do{
        if sender.state == .began{
            view.backgroundColor = .black
            ArticleViewImg.isHidden = true
        }
        if sender.state == .changed{
            sender.view?.transform = (sender.view?.transform)!.scaledBy(x: sender.scale, y: sender.scale)
            sender.scale = 1.0        }
        if sender.state == .ended{
            view.backgroundColor = .black
            
        }
        } catch let error{
            print(error)
        }
        
    }
    
}
