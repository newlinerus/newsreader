//
//  MyCell.swift
//  NewsReader
//
//  Created by George Heints on 30.08.17.
//  Copyright © 2017 George Heints. All rights reserved.
//

import UIKit

class MyCell: UICollectionViewCell {
    
    @IBOutlet weak var CellImageView: UIImageView!
}
