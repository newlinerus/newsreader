//
//  MenuViewController.swift
//  NewsReader
//
//  Created by George Heints on 30.08.17.
//  Copyright © 2017 George Heints. All rights reserved.
//

import UIKit
import RealmSwift
 
class MenuViewController: UIViewController,UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource{

    // MARK: - SegmentedMenuControl
    @IBAction func SegmentedMenuControl(_ sender: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            MenuTableView.isHidden = true
            MenuCollectionView.isHidden = false
        case 1:
            MenuTableView.isHidden = false
            MenuCollectionView.isHidden = true
        default:
            break; 
        }
    }
    
    // MARK: - Outlets
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var MenuTableView: UITableView!
    @IBOutlet weak var MenuCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            MenuTableView.isHidden = true
            fetchSources()
        }
        else {
            print("Internet connection FAILED")
            MenuTableView.isHidden = true
            fetchSources()
        }
        
    }
    var sources: [ArticleSources]? = []
    
    // MARK: - fetchSources()
    func fetchSources(){
        do{
            
            let urlRequest = URLRequest(url: URL(string: "https://newsapi.org/v1/sources?language=en")!)
            let task = URLSession.shared.dataTask(with: urlRequest){(data,response,error) in
                
                if error != nil{
                    
                    print(error)
                    return
                }
                
                self.sources = [ArticleSources]()
                
                do{

                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String : AnyObject]
                    
                    if let SourcesFromJson = json["sources"] as? [[String : AnyObject]]{
                        
                        for SourceFromJson in SourcesFromJson{
                            
                            let source = ArticleSources()
                            let realm = try! Realm()
                            try! realm.write {
                                realm.create(SrcLinkRealm.self, value: SourceFromJson, update: true)
                            }
                            if let name = SourceFromJson["name"] as? String, let id = SourceFromJson["id"] as? String, let  url = SourceFromJson["url"] as? String{
                                
                                source.SourceName = name
                                source.SourceId = id
                                source.SourceUrl = url
                                print("\(source.SourceId)")

                                

                            }
                            self.sources?.append(source)
                            
                            
                        }
                    }
                    
                    DispatchQueue.main.async {
                        
                        self.MenuTableView.reloadData()
                        self.MenuCollectionView.reloadData()
                    }
                    
                } catch let error{
                    
                    print(error)
                }
            }
            
            task.resume()
        } catch let error{
            print(error)
        }
    }
    
    let items = try! Realm().objects(SrcLinkRealm.self)
    var sectionNames: [String] {
        return Set(items.value(forKeyPath: "name") as! [String]).sorted()
    }

    // MARK: - TableView cellForRowAt
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        do{
            
        if Reachability.isConnectedToNetwork() == true {
            
            //Connection AVAILABLE
            if let SourceCell = tableView.dequeueReusableCell(withIdentifier: "SourceCell", for: indexPath) as? MenuCell {
                
                SourceCell.SourceLbl.text = self.sources?[indexPath.item].SourceName
                return SourceCell
                
            }else {
                return UITableViewCell()
            }
            
        }
        else {
            
            //Connection FAILED
            if let SourceCell = tableView.dequeueReusableCell(withIdentifier: "SourceCell", for: indexPath) as? MenuCell {
            
                SourceCell.SourceLbl.text = self.items[indexPath.item].name
                return SourceCell
            
            }else {
                return UITableViewCell()
            }
        }
        } catch let error{
            print(error)
        }
        
    }
    
    // MARK: - TableView numberOfSections
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        do{
        if Reachability.isConnectedToNetwork() == true {
            
            //Connection AVAILABLE
            return self.sources?.count ?? 0

        }
        else {
            
            //Connection FAILED
            print("\(sectionNames.count ?? 0)")
            return sectionNames.count ?? 0
            
        }
        } catch let error{
            print(error)
        }
    }
    
    // MARK: - TableView didSelectRowAt
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        do{
        
        if Reachability.isConnectedToNetwork() == true {
            
            //Connection AVAILABLE
            let partyRock = sources?[indexPath.row]
            performSegue(withIdentifier: "NewsViewController", sender: partyRock)
            
        }
        else {
            
            //Connection FAILED
            let partyRock = items[indexPath.row]
            print("\(partyRock)")
            performSegue(withIdentifier: "NewsViewController", sender: partyRock)

        }
        } catch let error{
            print(error)
        }
    }
    
    // MARK: - UIStoryboardSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? ViewController {
            
            if let partySource = sender as? ArticleSources {
                //destination.article = party
                destination.source = partySource
            }
            if let partySourceM = sender as? SrcLinkRealm {
                destination.getMark = partySourceM
            }
            
        }
    }

    // MARK: - CollectionView numberOfItemsInSection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        do{
        //Connection AVAILABLE
        if Reachability.isConnectedToNetwork() == true {
            
            return self.sources?.count ?? 0
            
        }
        else {
            
            //Connection FAILED
            print("\(self.items.count ?? 0)")
            return self.items.count ?? 0
        }
        }catch let error{
            print(error)
        }
    }
    
    // MARK: - CollectionView cellForItemAt
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        do{
        if Reachability.isConnectedToNetwork() == true {
            
           //Connection AVAILABLE
            if let SourceGridCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SourceGridCell", for: indexPath) as? MenuGridCell {
                
                SourceGridCell.SourceGridLbl.text = self.sources?[indexPath.item].SourceName
                
                return SourceGridCell
                
            }else {
                return UICollectionViewCell()
            }
            
        }
        else {
            
            //Connection FAILED
            if let SourceGridCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SourceGridCell", for: indexPath) as? MenuGridCell {
                
                SourceGridCell.SourceGridLbl.text = self.items[indexPath.item].name
                
                return SourceGridCell
                
            }else {
                return UICollectionViewCell()
            }
        }
        } catch let error{
            print(error)
        }
    }
    
    // MARK: - CollectionView didSelectItemAt
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        do{
        
        if Reachability.isConnectedToNetwork() == true {
            
            //Connection AVAILABLE
            let partyRock = sources?[indexPath.row]
            performSegue(withIdentifier: "NewsViewController", sender: partyRock)
            
        }
        else {
            
            //Connection FAILED
            print("Internet connection FAILED didSelectItemAt Collection")
            let partyRock = items[indexPath.row]
            performSegue(withIdentifier: "NewsViewController", sender: partyRock)
        }
        } catch let error{
            print(error)
        }
        
    }

}
