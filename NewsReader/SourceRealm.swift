//
//  SourceRealm.swift
//  NewsReader
//
//  Created by George Heints on 31.08.17.
//  Copyright © 2017 George Heints. All rights reserved.
//

import Foundation
import RealmSwift

class SrcLink {
    var id: String = ""
    var name: String = ""
    var descr: String = ""
}



class SrcLinkRealm : Object {
    dynamic var id: String = ""
    dynamic var name: String = ""
    dynamic var descr: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

