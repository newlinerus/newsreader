//
//  RealmObjects.swift
//  NewsReader
//
//  Created by George Heints on 31.08.17.
//  Copyright © 2017 George Heints. All rights reserved.
//

import Foundation
import RealmSwift

class ArticleData : Object{
    
    dynamic var Title = ""
    dynamic var Description = ""
    
    override class func primaryKey() -> String?{
        return "Title"
    }

}
