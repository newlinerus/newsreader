//
//  ViewController.swift
//  NewsReader
//
//  Created by George Heints on 30.08.17.
//  Copyright © 2017 George Heints. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!

    
    var refreshControl:UIRefreshControl!
    var articles: [Article]? = []
    
    let itemsArt = try! Realm().objects(Landmark.self)
    
    var sectionNamesArt: [String] {
        return Set(itemsArt.value(forKeyPath: "title") as! [String]).sorted()
    }

    // MARK: - Data from MenuViewController
    private var _Sources: ArticleSources!
    var source: ArticleSources{
        get{
            return _Sources
        } set{
            _Sources = newValue
        }
    }
    private var _mark : SrcLinkRealm!
    var getMark: SrcLinkRealm{
        get{
            return _mark
        } set{
            _mark = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            let Link: String = source.SourceId!
            fetchArticles(Link: Link)
            refreshControl = UIRefreshControl()
            refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            refreshControl.addTarget(self, action: #selector(ViewController.fetchArticles(Link:)), for: UIControlEvents.valueChanged)
            tableView.addSubview(refreshControl)
        }
        else {
            print("Internet connection FAILED")
            let realm = try! Realm()
            let Link = realm.objects(testRealm)
            let dog = realm.object(ofType: testRealm.self, forPrimaryKey: "cnn" as AnyObject)
            print("\(dog)")
        }

    }
    
    func pupulate(){
        tableView.reloadData()
    }
    // MARK: - fetchArticles()
    // if connection == connected
    func fetchArticles(Link: String){
        do{
        let urlRequest = URLRequest(url: URL(string: "https://newsapi.org/v1/articles?source=\(Link)&sortBy=top&apiKey=478c92812890474281617daa2793cf53")!)
        
        let task = URLSession.shared.dataTask(with: urlRequest){(data,response,error) in
            
            if error != nil{
                print(error)
                return
            }
            
            self.articles = [Article]()
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String : AnyObject]
                let realm = try! Realm()
                try! realm.write {
                    realm.create(
                        testRealm.self, value: json, update: true)
                    
                }
                if let articlesFromJson = json["articles"] as? [[String : AnyObject]]{
                    for articleFromJson in articlesFromJson{
                        
                        let article = Article()
                        let realm = try! Realm()
                        try! realm.write {

                            realm.create(Landmark.self, value: articleFromJson, update: true)
                        }
                        if let title = articleFromJson["title"] as? String, let descript = articleFromJson["description"] as? String, let imageToUrl = articleFromJson["urlToImage"] as? String{
                            
                            article.descript = descript
                            article.headline = title
                            article.imageUrl = imageToUrl
                        
                        }
                        self.articles?.append(article)
                    }
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    
                }

                
            } catch let error{
                print(error)
            }
        }
        
        task.resume()
        self.tableView.reloadData()
        } catch let error{
            print(error)
        }
        tableView.reloadData()
        //refreshControl.endRefreshing()

    }

    // MARK: - TableView cellForRowAt
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        do{
        
        if Reachability.isConnectedToNetwork() == true {
            
            print("Internet connection OK cellForRowAt")
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as? ArticleCell {
                
                cell.title.text = self.articles?[indexPath.item].headline
                cell.desc.text = self.articles?[indexPath.item].descript
                cell.imgView.downloadImg(from: (self.articles?[indexPath.item].imageUrl)!)
                return cell
                
            }else {
                return UITableViewCell()
            }
            
        }
        else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as? ArticleCell {
                
                let realm = try! Realm()
                let NewsMark: String = getMark.id
                let dog = realm.object(ofType: testRealm.self, forPrimaryKey: NewsMark as AnyObject)
                
                cell.title.text = dog?.articles[indexPath.item].title
                //cell.title.text = dog?.articles[indexPath.item].descript
                

                return cell
                
            }else {
                return UITableViewCell()
            }
        }
        } catch let error{
            print(error)
        }
        
    }
    
    // MARK: - TableView numberOfSections
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    // MARK: - TableView numberOfRowsInSection
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        do{
        if Reachability.isConnectedToNetwork() == true {
            
            print("Internet connection OK numberOfRowsInSection")
            
            return self.articles?.count ?? 0
            
        }
        else {
            
            print("Internet connection FAILED numberOfRowsInSection")
            
            print("\(sectionNamesArt.count ?? 0)")
            return sectionNamesArt.count ?? 0
            
        }
        } catch let error{
            print(error)
        }

    }
    
    // MARK: - TableView didSelectRowAt
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        do{
        if Reachability.isConnectedToNetwork() == true {
            
            print("Internet connection OK didSelectRowAt")
            let partyRock = articles?[indexPath.row]
            performSegue(withIdentifier: "ArticleViewController", sender: partyRock)
            
        }
        else {
            
            print("Internet connection FAILED didSelectRowAt")
            let partyRock = itemsArt[indexPath.row]
            performSegue(withIdentifier: "ArticleViewController", sender: partyRock)
            
        }
        } catch let error{
            print(error)
        }
    }
    
    // MARK: - UIStoryboardSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? ArticleViewController {
            
            if let party = sender as? Article {
                destination.article = party
            }
            
        }
    }
}


//required App Transport Security Settings
extension UIImageView{
    func downloadImg(from url: String){
        
        let urlRequest = URLRequest(url: URL(string: url)!)
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data,response,error) in
            
            if error != nil{
                print(error)
                return
            }
            
            DispatchQueue.main.async {
                self.image = UIImage(data: data!)
            }
        }
        task.resume()
    }
}
