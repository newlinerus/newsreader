//
//  StoredData.swift
//  NewsReader
//
//  Created by George Heints on 04.09.17.
//  Copyright © 2017 George Heints. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

// MARK: - Realm model
class testRealm : Object {
    dynamic var source: String = ""
    var articles = List<Landmark>()
    
    override static func primaryKey() -> String? {
        return "source"
    }

}

class Landmark : Object{
    dynamic var title: String = ""
    dynamic var descript: String = ""
    override static func primaryKey() -> String? {
        return "title"
    }
}
