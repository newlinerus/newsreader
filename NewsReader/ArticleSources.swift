//
//  ArticleSources.swift
//  NewsReader
//
//  Created by George Heints on 30.08.17.
//  Copyright © 2017 George Heints. All rights reserved.
//

import UIKit

// MARK: - temp storage
class ArticleSources: NSObject {
    
    var SourceId: String?
    var SourceName: String?
    var SourceImgUrl: String?
    var SourceUrl: String?
    var SourceSrc: String?

}
