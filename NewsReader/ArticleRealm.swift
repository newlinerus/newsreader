//
//  ArticleRealm.swift
//  NewsReader
//
//  Created by George Heints on 31.08.17.
//  Copyright © 2017 George Heints. All rights reserved.
//

import Foundation
import RealmSwift

class ArtLink {
    var source: String = ""
    var name: String = ""
    var descr: String = ""
    var url: String = ""
    var urlImg: String = ""
}



class ArtLinkRealm : Object {
    dynamic var source: String = ""
    dynamic var name: String = ""
    dynamic var descr: String = ""
    dynamic var url: String = ""
    dynamic var urlImg: String = ""
    
    
    override static func primaryKey() -> String? {
        return "source"
    }
}
